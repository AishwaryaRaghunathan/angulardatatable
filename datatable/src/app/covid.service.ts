import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CountryReports } from 'countryReports';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CovidService {
  constructor(private http:HttpClient) { }
  public covid19Reports(){
  return  this.http.get("https://corona.lmao.ninja/v2/countries");
  }

}